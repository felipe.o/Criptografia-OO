#ifndef PACKET_H
#define PACKET_H

#include "array.hpp"

#include <iostream>

typedef union union_size{
	int tamanho;
	char byte[4];
}size;


class Packet{
private:
	byte tag;
	unsigned short length;
	array::array *value;
	array::array *hash;
	Packet(const byte tag);
	Packet(const byte tag, const array::array *value);
	Packet(const array::array *packet);
	~Packet();
	void sock_send(const int sock);
	size_t tamanho();
	unsigned short swapBytes(unsigned short const x);
public:
	array::array *bytes();
	array::array static *Registration(const int);
	array::array static *Authentication(const int, array::array*);
	void static RequestObject(const int, array::array*, array::array*);
};

#endif