#ifndef KEYS_H
#define KEYS_H

#include "crypto.hpp"

class Keys{
private:
	RSA *server_public;
	RSA *client_public;
	RSA *client_private;
public:
	void load();
	void destroy();
	RSA* clientPublic();
	RSA* serverPublic();
	RSA* clientPrivate();
};

#endif