#include "keys.hpp"
#include "crypto.hpp"

void Keys::load(){
	this->client_public = crypto::rsa_read_public_key_from_PEM("doc/public.pem");
	this->server_public = crypto::rsa_read_public_key_from_PEM("doc/server_pk.pem");
	this->client_private = crypto::rsa_read_private_key_from_PEM("doc/private.pem");
}

void Keys::destroy(){
	crypto::rsa_destroy_key(client_private); 
	crypto::rsa_destroy_key(client_public); 
	crypto::rsa_destroy_key(server_public); 
}

RSA* Keys::clientPublic(){
	return client_public;
}

RSA* Keys::serverPublic(){
	return server_public;
}

RSA* Keys::clientPrivate(){
	return client_private;
}