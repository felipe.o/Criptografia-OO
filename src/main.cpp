#include "network.hpp"
#include "packet.hpp"
#include "array.hpp"

#include <iostream>

using namespace std;		

int main(int argc, char** argv) {
	int sock;

	cout << "Conectando..." << endl;

	if((sock = network::connect("45.55.185.4", 3000)) < 0){
		cout << "Erro ao conectar" << endl;
		return -1;
	}

	cout << "Conectado!" << endl;

	array::array *S_key = Packet::Registration(sock);

	array::array *token = Packet::Authentication(sock, S_key);

	Packet::RequestObject(sock, token, S_key);

	return 0;
}