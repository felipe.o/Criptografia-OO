#include "packet.hpp"
#include "crypto.hpp"
#include "network.hpp"
#include "keys.hpp"

#include <cstdio>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>

using namespace std;

// #define {0xaf, 0x3f, 0x32, 0xca, 0x85, 0x92, 0xc0, 0x88} MEU_ID

Packet::Packet(const byte tag, const array::array *value){
	this->tag = tag;
	this->length = value->length;
	this->value = array::copy(value);
	this->hash = crypto::sha1(value);
}

Packet::Packet(const byte tag){
	this->tag = tag;
	this->length = 0;
	if(value!=nullptr){
		array::destroy(value);
		array::destroy(hash);
	}
}

Packet::Packet(const array::array *packet){
	tag = packet->data[4];
	length = swapBytes(packet->length - 27);
	value = array::wrap(swapBytes(length), packet->data+7);
	hash = array::wrap(20, packet->data + length + 7);
}

Packet::~Packet(){
	tag = 0;
	length = 0;
	/*if(value!=nullptr){
		array::destroy(value);
		array::destroy(hash);
	}*/
}

// A parte e registro do processo
array::array* Packet::Registration(const int sock){

	/////////////////////// REQUEST_REGISTRATION: 0xC0 - Nenhum value //////////////////////////////////

	Packet *packet = new Packet(0xC0);
	packet->sock_send(sock);
	array::array *receive = network::read(sock);
	if(receive->data[4] != 0xC1){
		std::cout << "Erro no recebimento do pacote 0xC1" << std::endl;
		return nullptr;   
	}
	array::destroy(receive);

	/////////////////////// REGISTE_ID: 0xC2 - ID encriptado com PK ///////////////////////////

	Keys keys;
	keys.load();

	byte id[] = {0xaf, 0x3f, 0x32, 0xca, 0x85, 0x92, 0xc0, 0x88}; // Meu ID

	array::array *value = array::wrap(sizeof(id), id);

	array::array *encrypt_value = crypto::rsa_encrypt(value, keys.serverPublic());

	packet = new Packet(0xC2, encrypt_value);

	packet->sock_send(sock);

	receive = network::read(sock);

	// std::cout << "Recebido: " << *receive << std::endl;

	packet = new Packet(receive);

	// receive = packet->value;

	array::array *S_key = crypto::rsa_decrypt(packet->value, keys.clientPrivate());

	std::cout << "Chave simétrica: " << *S_key << std::endl; //<< *receive << std::endl;

	array::destroy(value);
	array::destroy(encrypt_value);
	keys.destroy();
	// delete[] id;

	return S_key;
}

// A parte de autenticação do processo
array::array* Packet::Authentication(const int sock, array::array* S_key){
	/////////////////////// Request_Auth: 0xA0 - ID encriptado com PK ///////////////////////////
	Keys keys;
	keys.load();


	byte id[] = {0xaf, 0x3f, 0x32, 0xca, 0x85, 0x92, 0xc0, 0x88}; // Meu ID
	array::array *value = array::wrap(sizeof(id), id);

	array::array *encrypt_value = crypto::rsa_encrypt(value, keys.serverPublic());

	Packet *packet = new Packet(0xA0, encrypt_value);

	packet->sock_send(sock);

	array::array *receive = network::read(sock);

	packet = new Packet(receive);

	array::array* token = crypto::rsa_decrypt(packet->value, keys.clientPrivate());


	keys.destroy();
	// array::destroy(receive);

	/////////////////////// REQUEST_CHALLENGE 0xA2 - vazio ///////////////////////////

	packet = new Packet(0xA2);

	packet->sock_send(sock);

	receive = network::read(sock);

	packet = new Packet(receive);

	array::array *packet_m = crypto::aes_decrypt(packet->value, token, S_key);

	packet = new Packet(0xA5, packet_m);
	packet->sock_send(sock);
	receive = network::read(sock);

	packet = new Packet(receive);

	token = crypto::aes_decrypt(packet->value, token, S_key);

	std::cout << "Token: " << *token << std::endl;

	return token;
}
/*
	0x20 0x35

	00000000 00010000 00000000 00100011
*/

// A parte de pegar a imagem do pc do cara
void Packet::RequestObject(const int sock, array::array *token, array::array *S_key){
	byte object[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
	array::array *value = array::wrap(sizeof(object), object);
	array::array *encrypt_value = crypto::aes_encrypt(value, token, S_key);

	Packet *packet = new Packet(0xB0, encrypt_value);
	packet->sock_send(sock);
	array::array *header = network::read(sock);

	short int length = (header->data[1] & 0xff) << 8 | (header->data[0] & 0xff);

	array::array *receive = network::read(sock, length);

	value = array::wrap(receive->length - 23, receive->data + 3);
	array::array *final = crypto::aes_decrypt(value, token, S_key);

	// std::cout  << *header << endl << *receive << std::endl << final->length << std::endl;

	ofstream fp;
	fp.open ("images.jpg");

	for(unsigned int i=0; i<final->length; i++)
		fp << final->data[i]; // << teste->data[i];
	fp.close();
	array::destroy(header);
	array::destroy(receive);
	array::destroy(encrypt_value);
	array::destroy(value);
}

size_t Packet::tamanho(){
	if(value == nullptr)
		return 3;
	else
		return 3 + value->length + 20;
}

array::array* Packet::bytes() {
	array::array* raw;
		
	if(this->length == 0) {
		raw = array::create(3);
	}
	else {
		raw = array::create(this->tamanho());
	}
	raw->data[0] = this->tag;
	raw->data[1] = this->length & 0xFF;
	raw->data[2] = (this->length >> 8) & 0xFF;

	if(this->length > 0) {
		for(int i=0; i<length; i++)
			raw->data[i+3] = value->data[i];
		for(int i=0; i<20; i++)
			raw->data[i+3+length] = hash->data[i];
	}

	// std::cout << *raw << std::endl;

	return raw;
}

void Packet::sock_send(const int sock){
	size_t size = this->tamanho();
	array::array* packet_bytes=this->bytes();

	// std::cout << *value << std::endl;

	byte *tmp = new byte[4];
	array::array *header = array::wrap(4, tmp);
	tmp[0] = size & 0xFF;
	tmp[1] = (size >> 8) & 0xFF;
	tmp[2] = (size >> 16) & 0xFF;
	tmp[3] = (size >> 24) & 0xFF;

	// std::cout << *header << std::endl << *packet_bytes << std::endl;

	network::write(sock, header);
	network::write(sock, packet_bytes);

	array::destroy(packet_bytes); array::destroy(header);
	delete[] tmp;
}

unsigned short Packet::swapBytes(unsigned short const x){
	char hibyte = (x & 0xff00) >> 8;
	char lobyte = (x & 0xff);
	short num = lobyte << 8 | hibyte;

	// printf("%X %X %X %X\n", hibyte, lobyte, num, x);



	return num;
}